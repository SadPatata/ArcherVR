using Autohand;
using Autohand.Demo;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class Summon : MonoBehaviour
{

    public GameObject sumonBow;
    private bool flag;
    private GameObject mBow;
    // Start is called before the first frame update
    void Start()
    {
        flag = false;
    }

    // Update is called once per frame
    void Update()
    {
        
       

    }

    public void summon()
    {
        if (!flag)
        {
            flag = true;
            mBow = Instantiate(sumonBow);
            mBow.transform.position = this.transform.position - new Vector3(this.transform.right.x *0.05f, this.transform.right.y*0.05f, this.transform.right.z*0.05f);
            mBow.transform.rotation = this.transform.rotation;

        }
        


    }

    public void banish()
    {
        if (flag) {
            new WaitForSeconds(2f);
            Destroy(mBow);
            flag = false;
        }
    }

}
