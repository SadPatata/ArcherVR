using Autohand;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Divide : MonoBehaviour
{
    private bool undividable;

    private void Start()
    {
        undividable = true;
        StartCoroutine("CD");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!undividable)
        {
            undividable = true;
            Break();
        }
        else
        {
            StartCoroutine("CD");
        }
            
    }

    public float force = 10f;
    Vector3[] offsets = { new Vector3(0.25f, 0.25f, 0.25f), new Vector3(-0.25f, 0.25f, 0.25f), new Vector3(0.25f, 0.25f, -0.25f), new Vector3(-0.25f, 0.25f, -0.25f),
                            new Vector3(0.25f, -0.25f, 0.25f), new Vector3(-0.25f, -0.25f, 0.25f), new Vector3(0.25f, -0.25f, -0.25f), new Vector3(-0.25f, -0.25f, -0.25f),};
    [ContextMenu("Break")]
     public void Break()
        {
            for (int i = 0; i < 8; i++)
            {
                var smallerCopy = Instantiate(gameObject, transform.position, transform.rotation);
                foreach (var joint in smallerCopy.GetComponents<FixedJoint>())
                {
                    Destroy(joint);
                }
                try
                {
                    smallerCopy.transform.parent = transform;
                }
                catch { }
                smallerCopy.transform.localPosition += offsets[i];
                smallerCopy.transform.parent = null;
                smallerCopy.transform.localScale = transform.localScale / 2f;
                var body = smallerCopy.GetComponent<Rigidbody>();
                body.ResetCenterOfMass();
                body.ResetInertiaTensor();
                body.velocity = GetComponent<Rigidbody>().velocity;
                body.AddRelativeForce(transform.rotation * (offsets[i] * force), ForceMode.Impulse);
                body.AddRelativeTorque(transform.rotation * (offsets[i] * force + Vector3.one * (Random.value / 3f)), ForceMode.Impulse);
                body.mass /= 2;

            }
            Destroy(gameObject);
       }


    IEnumerator CD()
    {
        yield return new WaitForSeconds(1f);
        undividable = false;
    }

}
