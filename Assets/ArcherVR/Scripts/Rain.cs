using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rain : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine("Divide");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void shooted()
    {
        StartCoroutine("Divide");
    }

    public float force = 10f;
    Vector3[] offsets = { new Vector3(0.25f, 0.25f, 0.25f), new Vector3(-0.25f, 0.25f, 0.25f), new Vector3(0.25f, 0.25f, -0.25f), new Vector3(-0.25f, 0.25f, -0.25f),
                            new Vector3(0.25f, -0.25f, 0.25f), new Vector3(-0.25f, -0.25f, 0.25f), new Vector3(0.25f, -0.25f, -0.25f), new Vector3(-0.25f, -0.25f, -0.25f),
                            new Vector3(0.45f, 0.45f, 0.45f), new Vector3(-0.45f, 0.45f, 0.45f), new Vector3(0.45f, 0.45f, -0.45f), new Vector3(-0.45f, 0.45f, -0.45f),
                            new Vector3(0.45f, -0.45f, 0.45f), new Vector3(-0.45f, -0.45f, 0.45f), new Vector3(0.45f, -0.45f, -0.45f), new Vector3(-0.45f, -0.45f, -0.45f),};
    [ContextMenu("Break")]
    public void Break()
    {
        for (int i = 0; i < 16; i++)
        {
            var smallerCopy = Instantiate(gameObject, transform.position, transform.rotation);
            foreach (var joint in smallerCopy.GetComponents<FixedJoint>())
            {
                Destroy(joint);
            }
            try
            {
                smallerCopy.transform.parent = transform;
            }
            catch { }
            smallerCopy.transform.localPosition += offsets[i];
            smallerCopy.transform.parent = null;
            smallerCopy.transform.localScale = transform.localScale / 2f;
            var body = smallerCopy.GetComponent<Rigidbody>();
            body.ResetCenterOfMass();
            body.ResetInertiaTensor();
            body.velocity = GetComponent<Rigidbody>().velocity;
            body.AddRelativeForce(transform.rotation * (offsets[i] * force), ForceMode.Impulse);
            body.AddRelativeTorque(transform.rotation * (offsets[i] * force + Vector3.one * (Random.value / 3f)), ForceMode.Impulse);
            body.mass /= 2;

        }
        Destroy(gameObject);
    }

    IEnumerator Divide()
    {
        yield return new WaitForSeconds(1f);
        Break();
    }

}
