using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    public bool flag;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Bob");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Bob()
    {
        while (true)
        {
            if (flag)
            {              
                this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(200, 0, 0));
                flag = false;
            }
            else
            {
                this.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-200, 0, 0));
                flag = true;
            }

            yield return new WaitForSeconds(4f);

        }
    }
}
