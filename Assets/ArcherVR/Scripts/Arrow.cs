using Autohand.Demo;
using Autohand;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Arrow : MonoBehaviour
{
    private float explosionForce = 3000;
    private float explosionRadius = 10;
    private ParticleSystem mParticles;
    private AudioSource mSound;
    public Transform mForward;

    // Start is called before the first frame update
    void Start()
    {
       StartCoroutine("Follow");
       mParticles = this.GetComponent<ParticleSystem>();
       mSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.forward = Vector3.Slerp(this.transform.forward, this.GetComponent<Rigidbody>().velocity.normalized, Time.deltaTime);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(this.gameObject.tag == "Boom")
        {
            Explode();
        }
        else
        {
            StartCoroutine("Die");          
        }
        
      
    }

    IEnumerator Follow()
    {

        while (true)
        {

            this.transform.LookAt(mForward.position);
            //print("AAAAAAAAA");
            yield return new WaitForSeconds(0.05f);
        }
    }

    void Explode()
    {

        this.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        ParticleSystem.MainModule pmain = mParticles.main;
        mParticles.lights.light.color = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
        pmain.startColor = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
        mParticles.Play();
        mSound.Play();
        var hits = Physics.OverlapSphere(this.transform.position, explosionRadius);
        foreach (var hit in hits)
        {
            if (AutoHandPlayer.Instance.body == hit.attachedRigidbody)
            {
                AutoHandPlayer.Instance.DisableGrounding(0.05f);
                var dist = Vector3.Distance(hit.attachedRigidbody.position, this.transform.position);
                explosionForce *= 2;
                hit.attachedRigidbody.AddExplosionForce(explosionForce - explosionForce * (dist / explosionRadius), this.transform.position, explosionRadius);
                explosionForce /= 2;
            }
            if (hit.attachedRigidbody != null)
            {
                var dist = Vector3.Distance(hit.attachedRigidbody.position, this.transform.position);
                hit.attachedRigidbody.AddExplosionForce(explosionForce - explosionForce * (dist / explosionRadius), this.transform.position, explosionRadius);
            }
        }

        StartCoroutine("Die");

    }
    IEnumerator Die()
    {

            yield return new WaitForSeconds(3f);
            Destroy(this.gameObject);

    }

}
