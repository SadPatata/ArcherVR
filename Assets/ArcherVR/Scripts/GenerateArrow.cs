using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;


public class GenerateArrow : MonoBehaviour
{
    
    public GameObject mArrow;
    public GameObject mArrowDiv;
    private GameObject selectedArrow;
    private bool flag;
    public GameObject mBow;
    public float mMaxForce;
    public GameObject mStart;   
    private GameObject holdedArrow;

    // Start is called before the first frame update
    void Start()
    {
        selectedArrow = mArrow;
        flag = false;
    }

    // Update is called once per frame
    void Update()
    {

        //this.transform.LookAt(mBow.transform.position);

    }

    public void changeArrow() {

        if (flag)
        {
            selectedArrow = mArrow;
            flag = false;
        }
        else
        {
            selectedArrow = mArrowDiv;
            flag = true;
        }

    }

    public void generate()
    {

        GameObject newArrow = Instantiate(selectedArrow);    
        newArrow.transform.position = new Vector3(mStart.transform.position.x, mStart.transform.position.y, mStart.transform.position.z);
        newArrow.transform.LookAt(mBow.transform.position);
        newArrow.GetComponent<Rigidbody>().useGravity = false;
        newArrow.transform.parent = mStart.transform;
        newArrow.GetComponent<Arrow>().mForward = mBow.transform;   
        holdedArrow = newArrow;
        StartCoroutine("Aim");

    }

    public void shoot()
    {
        if(holdedArrow.transform != null)
        {
            StopCoroutine("Aim");
            holdedArrow.GetComponent<LineRenderer>().GetComponent<LineRenderer>().enabled = false;
            holdedArrow.GetComponent<Rigidbody>().useGravity = true;        
            holdedArrow.transform.parent = null;
            holdedArrow.GetComponent<Arrow>().StopAllCoroutines();
            float shootForce = Vector3.Distance(mBow.transform.position, mStart.transform.position)*100;
            //print(shootForce);
            holdedArrow.GetComponent<Rigidbody>().AddRelativeForce(0,0, mMaxForce/100 * shootForce);       
        }

        if (flag)
        {
            holdedArrow.GetComponent<Rain>().shooted();
        }
    }

    IEnumerator Aim()
    {
        if (holdedArrow.transform != null)
        {
            while (true)
            {
                holdedArrow.GetComponent<LineRenderer>().startWidth = Vector3.Distance(mBow.transform.position, mStart.transform.position)/20* 2;
                //print(holdedArrow.GetComponent<LineRenderer>().startWidth);
                yield return new WaitForSeconds(0.3f);
            }
        }
    }
}
